import pyaudio
import pygame
import struct
from scipy.io import wavfile as wav
from scipy.fftpack import fft
import math
import sys
from collections import deque


NoteFrequencies = [ ("OutOfRange",-1), ("C",32.7),("C#",34.6),("Re",36.7),("Re#",38.9),("Mi",41.2),("Fa",43.7),("Fa#",46.2),("Sol",49),("Sol#",51.9),("La",55),("La#",58.3),("Si",61.7),("Do",65.4),("Do#",69.3),("Re",73.4),("Re#",77.8),
                    ("Mi",82.4),("Fa",87.3),("Fa#",92.5),("Sol",98),("Sol#",103.8),
                    ("La",110),("La#",116.5),("Si",123.5),("Do",130.8),("Do#",138.6),
                    ("Re",146.8),("Re#",155.6),("Mi",164.8),("Fa",174.6),("Fa#",185),
                    ("Sol",196),("Sol#",207.7),("La",220),("La#",233.1),
                    ("Si",246.9),("Do",261.6),("Do#",277.2),("Re",293.7),("Re#",311.1),
                    ("Mi",329.6),
                    ("Fa",349.2),("Fa#",370),("Sol",392),("Sol#",415.3),("La",440),("La#",466.2),("Si",493.9),("Do",523.3),("Do#",554.4),("Re",587.3),("Re#",622.3),("Mi",659.3),("Fa",698.5),("Fa#",740),("Sol",784),("Sol#",830),("OutOfRange",sys.maxsize)]


def listDevice(pyAudio):
    deviceCount = pyAudio.get_device_count()
    i = 0
    while i < deviceCount:
        infos = pyAudio.get_device_info_by_index(i)
        #for k in infos.keys():
        #    print(k,"\t",str(infos[k]))
        print("["+str(i)+"] " + str(infos["name"]))
        i = i + 1


PYAUDIO_RECORDER = None
PYAUDIO_RECORDER_STREAM = None
PYAUDIO_RECORDER_WIDTH = 2
PYAUDIO_RECORDER_CHANNELS = 1
PYAUDIO_RECORDER_RATE = 44100
PYAUDIO_RECORDER_BUFFER_LENGTH = 1  #length of the buffer in second
PYAUDIO_RECORDER_BUFFER = []#deque(maxlen=PYAUDIO_RECORDER_RATE*PYAUDIO_RECORDER_BUFFER_LENGTH)
PYAUDIO_RECORDER_BUFFER2 = []#deque(maxlen=PYAUDIO_RECORDER_RATE*PYAUDIO_RECORDER_BUFFER_LENGTH)

def getRecordingBufferLength():
    global PYAUDIO_RECORDER_BUFFER
    return len(PYAUDIO_RECORDER_BUFFER) 

def getRecordingBuffer():
    global PYAUDIO_RECORDER_BUFFER,PYAUDIO_RECORDER_BUFFER2
    # PYAUDIO_RECORDER_BUFFER2.clear()
    # b = PYAUDIO_RECORDER_BUFFER
    # PYAUDIO_RECORDER_BUFFER = PYAUDIO_RECORDER_BUFFER2
    # return b
    return PYAUDIO_RECORDER_BUFFER

def callbackRecording(in_data, frame_count, time_info, status):
    global PYAUDIO_RECORDER_BUFFER
    #for item in in_data: PYAUDIO_RECORDER_BUFFER.append(item)
    PYAUDIO_RECORDER_BUFFER = in_data
    return (in_data, pyaudio.paContinue)

def startRecording():
    global PYAUDIO_RECORDER,PYAUDIO_RECORDER_STREAM
    PYAUDIO_RECORDER = pyaudio.PyAudio()
    PYAUDIO_RECORDER_STREAM = PYAUDIO_RECORDER.open(format=PYAUDIO_RECORDER.get_format_from_width(PYAUDIO_RECORDER_WIDTH),
                channels=PYAUDIO_RECORDER_CHANNELS,
                rate=PYAUDIO_RECORDER_RATE,
                input=True,
                output=False,
                frames_per_buffer=PYAUDIO_RECORDER_RATE*PYAUDIO_RECORDER_BUFFER_LENGTH, #TODO use WIDTH ??
                stream_callback=callbackRecording)
    PYAUDIO_RECORDER_STREAM.start_stream()

def stopRecording():
    global PYAUDIO_RECORDER,PYAUDIO_RECORDER_STREAM
    PYAUDIO_RECORDER_STREAM.stop_stream()
    PYAUDIO_RECORDER_STREAM.close()
    PYAUDIO_RECORDER.terminate()



def lookupFrequencyName(freq):
    i = 1
    while i < len(NoteFrequencies)-1:
        fiMinus1 = NoteFrequencies[i-1]
        fiPlus1 =  NoteFrequencies[i+1]
        if freq > fiMinus1[1] and freq < fiPlus1[1]:
            return (NoteFrequencies[i-1],NoteFrequencies[i],NoteFrequencies[i+1])
        i = i +  1
    return None

def octetsEnNombres(data):
    data16=[]
    i = 0
    n = len(data)
    while i < n:
        data16.append( struct.unpack_from("h", data, offset=i)[0] )
        i = i  + 2
    return data16    

# def readSamplesFromCard(nthSeconds):
#     for i in range(0, int(RATE / CHUNK * nthSeconds)):
#         data = stream.read(CHUNK)
#     return data

def findFrequency(samples,rate):
    n = len(samples)
    frequencies = fft(samples)
    m = 0
    im = -1
    i = 1
    fSorted = []
    while i < len(frequencies):
        if m < abs(frequencies[i]):
            m = abs(frequencies[i])
            im = i
            fSorted.append(round(i * (rate/n))) 
        i = i + 1
    f = im * (rate/n)
    print(fSorted)
    return f


def drawTuner(freq):
    global fenetre
    tunerCentreX = 300
    tunerCentreY = 175
    tunerRayonX = 250
    tunerRayonY = 75
    startAngle = PI/4
    endAngle =  3*PI/4

    fNames = ("Error","Error","Error")
    fNormalizedLinearDeviation = 0.0 #[-1,1]
    fFreqNames = lookupFrequencyName(freq)
    if fFreqNames != None:
        fNames = (fFreqNames[0][0],fFreqNames[1][0],fFreqNames[2][0])
        fMin = fFreqNames[0][1]
        fMax = fFreqNames[2][1]
        #fRatio = (freq-fFreqNames[0][1]) / (fFreqNames[2][1]-fFreqNames[0][1]) #TODO: wrong
        fNormalizedLinearDeviation = 1200*math.log2(fFreqNames[1][1]/freq) / -100
        ##print(fRatio)

    pygame.draw.arc( fenetre, (255,255,255) , (tunerCentreX - tunerRayonX,tunerCentreY-tunerRayonY,2*tunerRayonX,2*tunerRayonY) , startAngle, endAngle, 4) 

    alpha = 6*PI/4 + ((PI/4) * fNormalizedLinearDeviation)
    #alpha =  5*PI/4 #7*PI/4
    x = round(tunerCentreX + tunerRayonX * math.cos(alpha))
    y = round(tunerCentreY + tunerRayonY * math.sin(alpha))
    pygame.draw.line( fenetre, (255,255,255), (x,y), (tunerCentreX,tunerCentreY))
    alpha =  5*PI/4
    while alpha <= 7*PI/4:
        x1 = round(tunerCentreX + (tunerRayonX-5) * math.cos(alpha))
        y1 = round(tunerCentreY + (tunerRayonY-5) * math.sin(alpha))
        x2 = round(tunerCentreX + (tunerRayonX+5) * math.cos(alpha))
        y2 = round(tunerCentreY + (tunerRayonY+5) * math.sin(alpha))
        pygame.draw.line( fenetre, (255,255,255), (x1,y1), (x2,y2))
        alpha = alpha + (7*PI/4 - 5*PI/4)/4
    yOffset = 24
    x = round(tunerCentreX + (tunerRayonX+yOffset) * math.cos(5*PI/4))
    y = round(tunerCentreY + (tunerRayonY+yOffset) * math.sin(5*PI/4))
    surfaceTexte = arial24.render(fNames[0],True,pygame.Color(0,255,0))
    fenetre.blit(surfaceTexte, (x-surfaceTexte.get_width()//2,y) )
    x = round(tunerCentreX + (tunerRayonX+yOffset) * math.cos(6*PI/4))
    y = round(tunerCentreY + (tunerRayonY+yOffset) * math.sin(6*PI/4))
    surfaceTexte = arial24.render(fNames[1],True,pygame.Color(0,255,0))
    fenetre.blit(surfaceTexte, (x-surfaceTexte.get_width()//2,y) )
    x = round(tunerCentreX + (tunerRayonX+yOffset) * math.cos(7*PI/4))
    y = round(tunerCentreY + (tunerRayonY+yOffset) * math.sin(7*PI/4))
    surfaceTexte = arial24.render(fNames[2],True,pygame.Color(0,255,0))
    fenetre.blit(surfaceTexte, (x-surfaceTexte.get_width()//2,y) )
        

def dessiner():
    global fenetre, dataSamples,lastFrequency
    fenetre.fill( (0,0,0) )
    average = 0
    for e in dataSamples:
        average = average + abs(e)
    if average != 0:
        average = average / len(dataSamples)
    scaledValue = average / 2**16 #TODO
    pygame.draw.rect( fenetre, (255 * scaledValue,(1-scaledValue)*255,0), (275,500,50,(-300*scaledValue))) 

    if lastFrequency != -1: 
        surfaceTexte = arial24.render("Frequence: " + str(round(lastFrequency)) + " Hz",True,pygame.Color(0,255,0))
        fenetre.blit(surfaceTexte, (10,10) )

        drawTuner(lastFrequency)
    pygame.display.flip()

pygame.init() # initialisation du module "pygame"
fenetre = pygame.display.set_mode( (600,600) )
arial24 = pygame.font.SysFont("arial",24)

PI = 3.1415


# WIDTH = 2
# CHANNELS = 1  
# RATE = 44100
# RECORD_SECONDS = 0.5
# CHUNK = round(RATE * RECORD_SECONDS)  #44100 / 2 #1024

# p = pyaudio.PyAudio()
# listDevice(p)
# stream = p.open(format=p.get_format_from_width(WIDTH),
#                 channels=CHANNELS,
#                 rate=RATE,
#                 input=True,
#                 output=True,
#                 frames_per_buffer=CHUNK)

dataSamples = []
lastFrequency = -1

startRecording()
print("* recording")

continuer = True
while continuer == True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            continuer = False
    #print("* recording")
    #dataChunk = readSamplesFromCard(RECORD_SECONDS)
    n = getRecordingBufferLength()
    if n >= PYAUDIO_RECORDER_RATE/3: #TODO: use WIDTH and/or LENGTH ??        
        dataChunk = getRecordingBuffer()
        dataSamples = octetsEnNombres(dataChunk)
        lastFrequency = findFrequency(dataSamples,PYAUDIO_RECORDER_RATE);
    #print("* writing")
    #stream.write(dataChunk, CHUNK)
    #print("* drawing")
    dessiner()


print("* done")
stopRecording()

#stream.stop_stream()
#stream.close()
#p.terminate()
